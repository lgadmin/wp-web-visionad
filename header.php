<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<div class="logo">
				<?php
					if(shortcode_exists('lg-site-logo')){
						echo do_shortcode('[lg-site-logo]');
					}else{
						the_custom_logo();
					}
			 	?>
			</div>

		 	<div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">

  		<?php wp_nav_menu( array(
        	'theme_location'              => 'top-nav',
    		'depth'             => 2,
    		'container'         => 'div',
    		'container_class'   => 'collapse navbar-collapse',
    		'container_id'      => 'main-navbar',
    		'menu_class'        => 'nav navbar-nav',
    		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
    		'walker'            => new WP_Bootstrap_Navwalker_Custom()
    		)
      	); 

      	?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
