<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="bg-gray clearfix pb-lg pt-lg">
			<div class="nav-footer"><?php get_template_part("/templates/template-parts/nav-footer"); ?></div>
			<div class="blog-feed"><code>Blog Feed</code></div>
			<div class="footer-utility"><?php get_template_part("/templates/template-parts/address-card"); ?></div>
		</div>
		<div class="bg-gray-base clearfix">
			<div class="site-info"><?php get_template_part("/templates/template-parts/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/templates/template-parts/site-footer-longevity"); ?> </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
