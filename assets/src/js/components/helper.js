function repositionStickyHeader(){
	var headerHeight = $('#masthead').outerHeight();
	$('#content').css('margin-top', headerHeight);
}

function headerScrollTo(toElement, settings){
	if($(toElement)[0]){
		var currentTop = $(window).scrollTop();
		var elementTop = $(toElement).offset().top;

		console.log(settings.duration);

		$('html, body').stop().animate({
	        scrollTop: elementTop + settings.offset
	    }, settings.duration);
	}
}